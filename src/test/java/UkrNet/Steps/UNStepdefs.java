package UkrNet.Steps;

import UkrNet.Mapping.*;
import UkrNet.runners.UNRunner;
import cucumber.api.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by andrus on 5/31/2016.
 */
public class UNStepdefs {
    public static String BaseURL_IE;
    public ResultPage resultPage;
    public static StartPage startPage;

    @Given("^I on UkrNet page$")
    public void iOnUkrNetPage() throws Throwable {
        Assert.assertTrue("Строка поиска не отображается.",UNRunner.startPage.TextBox_Search.isDisplayed());
        Assert.assertTrue("Кнопка 'Search' не отображается.",UNRunner.startPage.Button_Search.isDisplayed());
    }

    @When("^I am enter search text \"([^\"]*)\"$")
    public void iAmEnterSearchText(String arg0) throws Throwable {
        UNRunner.startPage.TextBox_Search.clear();
        UNRunner.startPage.TextBox_Search.sendKeys(UNRunner.startPage.SearchString);
    }

    @And("^I am click the button \"([^\"]*)\"$")
    public void iAmClickTheButton(String arg0) throws Throwable {
        resultPage = UNRunner.startPage.ButtonSearchClick();
    }

    @Then("^I am see more then first (\\d+) results on page$")
    public void iAmSeeMoreThenFirstResultsOnPage(int arg0) throws Throwable {
        Assert.assertTrue("Text with search results at result page is not visible.", resultPage.FoundText.isDisplayed());
        String tmp=resultPage.FoundText.getText().split(" ")[0];//Number of results (showed on top of Results page).
        Assert.assertTrue("Resume size is less than 5. And equals to:"+tmp, Integer.parseInt(tmp)>5);
        int Size = resultPage.ResultsList.size();//Number of results on Results page.
        Assert.assertTrue("Resume size is less than 5. And equals to:"+Size, resultPage.ResultsList.size()>5);
        List<WebElement> ResultsTextList;
        ResultsTextList=resultPage.ResultsList;
        ArrayList<String> ResultStrings = new ArrayList<>();//List of result names.
        for (WebElement el:ResultsTextList) {
            ResultStrings.add(el.getText().toString());
        }
        Assert.assertTrue("Resume size is less than 5. And equals to:"+ResultsTextList.size(), ResultsTextList.size()>5);
        UNRunner.driverFF.close();
    }

    @Given("^I on MailRu page$")
    public void iOnMailRuPage() throws Throwable {
        BaseURL_IE = "https://www.mail.ru/";
        UNRunner.driverIE = new FirefoxDriver();
        UNRunner.driverIE = new InternetExplorerDriver();
        UNRunner.driverIE.manage().window().maximize();
        UNRunner.driverIE.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        startPage = new StartPage(UNRunner.driverIE);
        UNRunner.driverIE.get(BaseURL_IE);
        UNRunner.driverIE.findElements(By.xpath(".//a"));
        UNRunner.driverIE.findElements(By.xpath(".//div"));
        //!!UNRunner.driverIE.close();
        //wait(500);
        /*        UNRunner.driverIE.findElements(By.xpath(".//font"));
/**/
        //UNRunner.driverIE.wait(10000);
//        WebDriver driver1;
/*        System.setProperty("webdriver.ie.driver", "D:\\IEDriverServer.exe");
        InternetExplorerDriver driver1;
        driver1=new InternetExplorerDriver();
        PageFactory.initElements(driver1, this);
        BaseURL = "https://www.mail.ru/";
        driver1.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);*/
        //startPage = new StartPage(driver);
        //driver1.get(BaseURL);
        //driver1.manage().window().maximize();
        //driver1.close();/**/
    }
}
