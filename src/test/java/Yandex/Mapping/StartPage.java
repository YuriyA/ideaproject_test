package Yandex.Mapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by andrus on 5/31/2016.
 */
public class StartPage {
    static public WebDriver driver;
    public StartPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "hplogo")
    static public WebElement Image_Logo;//Start page.
    @FindBy(xpath= ".//input[@type='text']")
    static public WebElement TextBox_Search;
    @FindBy (xpath = ".//center/input[1]")
    static public WebElement Button_Search;
}
