package com.example.Navigate;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy; // Obligatory for @FindBy
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created on 5/16/2016.
 */
public class StartPage {
    public static String BaseURL;
    private static String LogoXpath;
    private static WebElement LogoElement;
    public static String SearchString="Test";
    static public WebDriver driver;
    public ResultsPage ResultsPage;

    public StartPage()  {
        try {
            driver = new FirefoxDriver();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @Before
    public void setUp(){
        BaseURL = "http://ya.ru";
        LogoXpath = "html/body/table/tbody/tr[3]/td/a";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(BaseURL);
        driver.manage().window().maximize();
    }

    @Test
    public void TestYandex() throws InterruptedException {
        LogoElement = driver.findElement(By.xpath(LogoXpath));
        assertTrue("Expected element is visible at page.",LogoElement.isDisplayed() );
        ResultsPage.Image_Logo.isDisplayed();
    }

    @FindBy (xpath = "html/body/table/tbody/tr[3]/td/a")
    @CacheLookup
    static public WebElement Image_Logo;

    @FindBy (id = "text")
    @CacheLookup
    static public WebElement TextBox_Search;

    @FindBy (xpath = "html/body/table/tbody/tr[2]/td/form/div[2]/button")
    @CacheLookup
    static public WebElement Button_Search;

    @FindBy (xpath = "//div[@class='search2__button']")
    @CacheLookup
    static public WebElement ResultButton_Search2;

    static void SearchTest(WebDriver driver) throws InterruptedException {
        assertTrue("Expected element is visible at page.",Image_Logo.isDisplayed() );
        TextBox_Search.clear();
        TextBox_Search.sendKeys(SearchString);
        Button_Search.click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        assertTrue("Search button at result page is visible.",ResultButton_Search2.isDisplayed());
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Ignore
    public void Test3(){}
}