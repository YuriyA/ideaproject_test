package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import regex.RegExMethods;
import java.util.*;
import static org.junit.Assert.assertTrue;

/**
 * Created on 6/10/2016.
 */
public class MainPage {
    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    WebDriver driver;

    @FindBy(xpath = ".//a[contains(text(),'Apple')]")
    public WebElement RozetkaAppleElement;
    @FindBy(xpath = ".//div[@class='logo']/img")
    public WebElement RozetkaLogoElement;
    @FindBy(xpath = ".//*[@title='Телефоны, MP3']")
    public WebElement RozetkaMP3Element;
    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement CityChooserMenu;
    @FindBy(partialLinkText = "Харьков")
    public WebElement CityKharkov;
    @FindBy(partialLinkText = "Одесса")
    public WebElement CityOdessa;
    @FindBy(partialLinkText = "Киев")
    public WebElement CityKyiv;
    @FindBy(partialLinkText = "Корзина")
    public WebElement Bin;
    @FindBy(xpath = ".//h2[text()='Корзина пуста']")
    public WebElement BinEmpty;
    @FindBy(xpath = ".//*[@id='m-main']/li/a")
    public WebElement table;
    @FindBy(xpath = ".//div[@class='g-price-uah']")
    public WebElement prices;


    public void TestLogo() /*throws InterruptedException */{
        System.out.println("Rozetka tests are started.");
        assertTrue("Rozetka logo element is not visible at page.", RozetkaLogoElement.isDisplayed());
    }

    public void TestApple() /*throws InterruptedException*/ {
        assertTrue("Apple menu item element is not visible at page.", RozetkaAppleElement.isDisplayed());}

    @FindBy(xpath = ".//div[@class='g-title g-title-elipsis']")
    public List<WebElement> salaryOffers;

    public boolean findOfferMoreThen(int minSalary) {
        int i=0;
        for(WebElement elem : salaryOffers) {
            List<String> list = RegExMethods.getMatchedArray(elem.getText(), "\\d+.\\d+");
            for(String salary : list) {
                StringBuffer sb = new StringBuffer(salary);
                if (sb.indexOf(" ")>0)
                sb.deleteCharAt(sb.indexOf(" "));
                if(minSalary <= Integer.valueOf(sb.toString())){
                    System.out.print("Offer with " + salary + " is present!");
                    return true;
                }
            }
        }
        return false;
    }

    public void TestMP3()  {
        assertTrue("Menu elements with 'MP3' text was not found.", RozetkaMP3Element.isDisplayed());
    }

    public void TestCityNames() {
        assertTrue("City Chooser Menu is not displayed.", CityChooserMenu.isDisplayed());
        CityChooserMenu.click();
        CityKharkov.isDisplayed();
        CityOdessa.isDisplayed();
        CityKyiv.isDisplayed();
    }

    public void TestBinContents() {
        assertTrue("Bin link is not displayed.", Bin.isDisplayed());
        Bin.click();
        BinEmpty.isDisplayed();
        System.out.println("Rozetka tests are finished.");
    }

}