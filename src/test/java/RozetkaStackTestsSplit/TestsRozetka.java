package RozetkaStackTestsSplit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import java.util.concurrent.TimeUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

/**
 * Created on 6/10/2016.
 */
public class TestsRozetka {
    public TestsRozetka() {
        String RozetkaURL;
        RozetkaURL = "http://rozetka.com.ua/";
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(RozetkaURL);
        driver.manage().window().maximize();
        PageFactory.initElements(driver, this);
    }
    WebDriver driver;

    @FindBy(xpath = ".//a[contains(text(),'Apple')]")
    public WebElement RozetkaAppleElement;
    @FindBy(xpath = ".//div[@class='logo']/img")
    public WebElement RozetkaLogoElement;
    @FindBy(partialLinkText = "MP3")
    public WebElement RozetkaMP3Element;
    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement CityChooserMenu;
    @FindBy(partialLinkText = "Харьков")
    public WebElement CityKharkov;
    @FindBy(partialLinkText = "Одесса")
    public WebElement CityOdessa;
    @FindBy(partialLinkText = "Киев")
    public WebElement CityKyiv;
    @FindBy(partialLinkText = "Корзина")
    public WebElement Bin;
    @FindBy(xpath = ".//h2[text()='Корзина пуста']")
    public WebElement BinEmpty;
    @FindBy(xpath = ".//*[@id='m-main']/li/a")
    public WebElement table;
    @FindBy(xpath = ".//div[@class='g-price-uah']")
    public WebElement prices;


    @Before
    public void setUp() {
    }

    @BeforeTest
    public void testSetUp() {
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @AfterTest
    public void testTearDown() {
    }

    @Test
        public void TestLogo() throws InterruptedException {
            System.out.println("Rozetka tests are started.");
            assertTrue("Rozetka logo element is not visible at page.", RozetkaLogoElement.isDisplayed());
        }
    @Test
        public void TestApple() throws InterruptedException {
            assertTrue("Apple menu item element is not visible at page.", RozetkaAppleElement.isDisplayed());}

    @Test
    public void TestRegularEx() throws InterruptedException {

            List<WebElement> AllText=driver.findElements(By.xpath(".//div[@class='g-price-uah']"));
            String text=" 1099 грн ";
            Pattern p = Pattern.compile("\\d+");
            Matcher matcher = p.matcher(text);
            Integer i=0;
            int count = 0;
            for (i=0;i<AllText.size();i++) {
                text = AllText.get(i).getText().replace(" ","").split(" ")[0];
                matcher = p.matcher(text);
            }
            while(matcher.find()) {
                count++;
                System.out.println("start(): "+matcher.start());
                System.out.println("end(): "+matcher.end());
            }
        }
    @Test
        public void TestMP3() throws InterruptedException {
            assertTrue("Menu elements with 'MP3' text was not found.", RozetkaMP3Element.isDisplayed());
        }
    @Test
        public void TestCityNames() throws InterruptedException {
            assertTrue("City Chooser Menu is not displayed.", CityChooserMenu.isDisplayed());
            CityChooserMenu.click();
            CityKharkov.isDisplayed();
            CityOdessa.isDisplayed();
            CityKyiv.isDisplayed();
        }
    @Test
        public void TestBinContents() throws InterruptedException {
            assertTrue("Bin link is not displayed.", Bin.isDisplayed());
            Bin.click();
            BinEmpty.isDisplayed();
            System.out.println("Rozetka tests are finished.");
        }
}