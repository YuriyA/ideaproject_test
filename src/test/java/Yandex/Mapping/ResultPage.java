package Yandex.Mapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by andrus on 5/31/2016.
 */
public class ResultPage {
    static public WebDriver driver;
    public ResultPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy (xpath = ".//*[@id='resultStats']")
    public WebElement FoundText;
    @FindBy (xpath = ".//div[2]/b[contains(text(),'antibiotics')]")
    public WebElement FoundDropdownTetracyclinesAntibioticsList;
}
