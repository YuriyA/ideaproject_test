import Rozetka.MainPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import java.util.concurrent.TimeUnit;

/**
 * Created on 6/10/2016.
 */
public class TestsRozetka {
    public WebDriver driver;
    public MainPage mainPage;

    public TestsRozetka() {
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        mainPage = new MainPage(driver);
    }

    @Before
    public void setUp() {
    }

    @BeforeTest
    public void testSetUp() {
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @AfterTest
    public void testTearDown() {
    }


    @Test
        public void Test1(){
        mainPage.TestLogo();
    }

    @Test
    public void Test2(){
        mainPage.TestApple();
    }

    @Test
    public void Test3(){
        mainPage.findOfferMoreThen(30000);
    }

    @Test
    public void Test4(){
        mainPage.TestMP3();
    }

    @Test
    public void Test5(){
        mainPage.TestCityNames();
    }

    @Test
    public void Test6(){
        mainPage.TestBinContents();
    }
}