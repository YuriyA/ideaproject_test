package com.nixsolutions.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;


/**
 * class _Test1
 *
 * V1
 *
 * 5/25/2016
 *
 * Created by andrus.
 */

public class _Test1
    {
        List<Integer> inputArrayGlobalEmpty,inputArrayGlobalEmpty1;
        static public WebDriver driver;
        long[][] results = new long[2][21];
        public _Test1()  {
        }

        List<Integer> ArrayFill(){
            List<Integer> arrToFill = new ArrayList<Integer>();
            for (int i=0;i<10000;i++) {
                int z= (int) ((Math.random() * 200)-100);
                arrToFill.add(z);
            }
        return arrToFill;
        };

        void bubbleSort(List<Integer> inputArray) {

            for (int i=0;i<10000-1;i++){
                for(int j =0;j<10000-1;j++)
                    if (inputArray.get(j)>inputArray.get(j+1)){
                        inputArray.set(j+1, inputArray.get(j+1)+inputArray.get(j));
                        inputArray.set(j, inputArray.get(j+1)-inputArray.get(j));
                        inputArray.set(j+1, inputArray.get(j+1)-inputArray.get(j));
                    }}
        }



        @Test
        public void Main()
        {
            for (int i=0;i<20;i++){
                inputArrayGlobalEmpty=ArrayFill();
                inputArrayGlobalEmpty1=ArrayFill();
                long beforeBubble = System.currentTimeMillis();
                bubbleSort(inputArrayGlobalEmpty);
                long afterBubble = System.currentTimeMillis();
                long diffBubble = afterBubble - beforeBubble;
                results[0][i]=diffBubble;
                if (i==0) results[0][20]+=diffBubble;
                else  results[0][20]=(results[0][20]+diffBubble)/2;

                long beforeArrays = System.currentTimeMillis();
                Arrays.sort(inputArrayGlobalEmpty1.toArray());
                long afterArrays = System.currentTimeMillis();
                long diffArrays = afterArrays - beforeArrays;
                results[1][i]=diffArrays;
                if (i==0) results[1][20]+=diffArrays;
                else results[1][20]=(results[1][20]+diffArrays)/2;
            }
            double average = 0;
        }

        public WebDriver SearchTest(WebDriver driver) throws InterruptedException {

            driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
            return driver;
        }

        public int print(int a, long b){return 0;};
        public long print(long b, int a){return 0;};
    }
