package UkrNet.Mapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import UkrNet.runners.UNRunner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by andrus on 5/31/2016.
 */
public class StartPage {
    static public WebDriver driver;

    public StartPage(WebDriver driver)
    {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "html/body/div[3]/div/div[2]/header/div[1]/a/img")
    public WebElement Image_Logo;//Start page.
    @FindBy(id= "search-input")
    public WebElement TextBox_Search;
    @FindBy (xpath = ".//*[@id='search-form']/div/div/div[2]/input")
    public WebElement Button_Search;

    public String SearchString="tetris";

    public ResultPage ButtonSearchClick()
    {
        Set<String>oldWindowsSet = UNRunner.driverFF.getWindowHandles();
        Button_Search.click();
        UNRunner.driverFF.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Set<String>newWindowsSet = UNRunner.driverFF.getWindowHandles();
        newWindowsSet.removeAll(oldWindowsSet);
        String newWindowHandle = newWindowsSet.iterator().next();
        UNRunner.driverFF.switchTo().window(newWindowHandle);/**/
        return new ResultPage(UNRunner.driverFF);
    }
}
