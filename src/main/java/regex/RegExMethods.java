package regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 5/17/2016.
 */
public class RegExMethods {

    public static List<String> getMatchedArray (String inputText, String pattern){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(inputText);
        List<String> list = new ArrayList<String>();
        while (m.find()) {
            list.add(inputText.substring(m.start(), m.end()));
        }
        return list;
    }
}