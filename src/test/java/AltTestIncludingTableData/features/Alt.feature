@smoketest
Feature: TestTest

  Scenario: Alt test
    Given I am on <page> page Alt
      | page       | query   | results |
      |  https://www.ukr.net/   |  tetris  |  5   |
      |  https://www.ukr.net/   |  test  |  10  |
    When I am enter search text <string> Alt
    And I am click the button "Пошук" Alt
    Then I am see more then first <results> results on page Alt
