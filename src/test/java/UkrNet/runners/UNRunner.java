package UkrNet.runners;
import UkrNet.Mapping.StartPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * Created by andrus on 5/31/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/smoketest", "json:target/cucumber.json"},
        features = "src/test/java/UkrNet/features",
        glue = {"UkrNet/Steps","java/UkrNet/Steps"},//"java/UkrNet/Steps",
        tags = "@smoketest")

public class UNRunner {

        public static WebDriver driverFF,driverIE;
        public static StartPage startPage;
        public static String BaseURL, BaseURL_IE;

        @BeforeClass
        public static void setupTimeout(){
            System.setProperty("webdriver.ie.driver", "D:\\IEDriverServer.exe");
            BaseURL = "https://www.ukr.net/";
//            BaseURL_IE = "https://www.mail.ru/";
            driverFF = new FirefoxDriver();
//            driverIE = new InternetExplorerDriver();
            driverFF.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            startPage = new StartPage(driverFF);
            driverFF.get(BaseURL);
            driverFF.manage().window().maximize();
        }

        @AfterClass
        public static void TearDown(){
            driverIE.close();
            Set<String> FFWindowsSet = UNRunner.driverFF.getWindowHandles();
            String newWindowHandle = FFWindowsSet.iterator().next();
            UNRunner.driverFF.switchTo().window(newWindowHandle);
            driverFF.close();
         }

}
