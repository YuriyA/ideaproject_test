package AltTestIncludingTableData.Mapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by andrus on 5/31/2016.
 */
public class ResultPage {
    static public WebDriver driver;

    public ResultPage(WebDriver driver)
    {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = "html/body/article/nav/div[1]/div[1]/span")
    public WebElement FoundText;
    @FindBys({@FindBy (xpath = "html/body/article/div[1]/div[2]/section/h3/a")})
    public List<WebElement> ResultsList;

}
