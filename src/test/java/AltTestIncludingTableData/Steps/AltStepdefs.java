package AltTestIncludingTableData.Steps;

import AltTestIncludingTableData.Mapping.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by andrus on 5/31/2016.
 */
public class AltStepdefs {
    public ResultPage resultPage;
    public static WebDriver driverFF;
    public static StartPage startPage;
    String page, string, results;

    @Given("^I am on <page> page Alt$")
    public void iAmOnPagePageAlt(DataTable lstemp) throws Throwable {
        page=lstemp.cells(0).toArray()[1].toString().replace('[',' ').split(",")[0].trim().toString();
        string=lstemp.cells(0).toArray()[1].toString().split(",")[1].trim().toString();
        results=lstemp.cells(0).toArray()[1].toString().replace(']',' ').split(",")[2].trim().toString();

        driverFF = new FirefoxDriver();
        driverFF.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        startPage = new StartPage(driverFF);
        driverFF.get(page);
        driverFF.manage().window().maximize();

        Assert.assertTrue("Строка поиска не отображается.",startPage.TextBox_Search.isDisplayed());
        Assert.assertTrue("Кнопка 'Search' не отображается.",startPage.Button_Search.isDisplayed());
    }
    @When("^I am enter search text <string> Alt$")
    public void iAmEnterSearchTextStringAlt() throws Throwable {
        startPage.TextBox_Search.clear();
        startPage.TextBox_Search.sendKeys(startPage.SearchString);
    }

    @And("^I am click the button \"([^\"]*)\" Alt$")
    public void iAmClickTheButtonAlt(String arg0) throws Throwable {
        resultPage = startPage.ButtonSearchClick();
    }

    @Then("^I am see more then first <results> results on page Alt$")
    public void iAmSeeMoreThenFirstResultsResultsOnPageAlt() throws Throwable {
        Assert.assertTrue("Text with search results at result page is not visible.", resultPage.FoundText.isDisplayed());
        String tmp=resultPage.FoundText.getText().split(" ")[0];//Number of results (showed on top of Results page).
        Integer ResultsNumber;
        ResultsNumber=Integer.parseInt(results);
        Assert.assertTrue("Resume size is less than 5. And equals to:"+tmp, Integer.parseInt(tmp)>ResultsNumber);
        int Size = resultPage.ResultsList.size();//Number of results on Results page.
        Assert.assertTrue("Resume size is less than 5. And equals to:"+Size, resultPage.ResultsList.size()>ResultsNumber);
        List<WebElement> ResultsTextList;
        ResultsTextList=resultPage.ResultsList;
        ArrayList<String> ResultStrings = new ArrayList<>();//List of result names.
        for (WebElement el:ResultsTextList) {
            ResultStrings.add(el.getText().toString());
        }
        Assert.assertTrue("Resume size is less than 5. And equals to:"+ResultsTextList.size(), ResultsTextList.size()>ResultsNumber);
    }

}
