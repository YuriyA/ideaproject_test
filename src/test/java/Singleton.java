/*
public class StaticBlockSingleton {

    private static StaticBlockSingleton instance;

    private StaticBlockSingleton(){}

    //блок статической инициализации с возможностью обработки исключительных ситуаций
    static{
        try{
            instance = new StaticBlockSingleton();
        }catch(Exception e){
            throw new RuntimeException("При создании объекта «Singleton» произошла ошибка");
        }
    }

    public static StaticBlockSingleton getInstance(){
        return instance;
    }
}/**/