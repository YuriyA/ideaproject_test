package com.example.Navigate;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.Given;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;
/**
 * Created by andrus on 5/25/2016.
 */

public class BDD extends PageInstance
    {
        public static String BaseURL;
        public static String SearchString="Test";
        static public WebDriver driver;

        public BDD()  {
            try {
                driver = new FirefoxDriver();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            PageFactory.initElements(driver, this);
        }

        @Before
        public void setUp(){
            BaseURL = "http://ya.ru";
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(BaseURL);
            driver.manage().window().maximize();
        }

        @FindBy (id = "text")
        static public WebElement TextBox_Search;
        @FindBy (xpath = "html/body/table/tbody/tr[2]/td/form/div[2]/button")
        static public WebElement Button_Search;
        @FindBy (xpath = "//div[@class='search2__button']")
        static public WebElement ResultButton_Search2;
        @FindBy (xpath = "html/body/table/tbody/tr[3]/td/a")
        static public WebElement Image_Logo;//Search page.
        @FindBy (xpath = "html/body/div[1]/div/div[1]/div[1]/div/a")
        public WebElement Image_LogoResult;//Results page.
        @FindBy (xpath = ".//div[@class='input__found input__found_visibility_visible']")
        public WebElement FoundText;

        @Given("I on Yandex Start page$")
        public void I_On_Yandex_Start_Page() throws InterruptedException
            {
                Assert.assertTrue("Логотип не отображается.",Image_Logo.isDisplayed());
                Assert.assertTrue("Строка поиска не отображается.",TextBox_Search.isDisplayed());
                Assert.assertTrue("Кнопка 'Search' не отображается.",Button_Search.isDisplayed());
            }
        @Then("I enter text 111 to textbox$")
        public void I_Enter_text_To_Textbox() throws InterruptedException
            {
//                assertTrue("Expected element is visible at page.",Image_Logo.isDisplayed() );
                TextBox_Search.clear();
                TextBox_Search.sendKeys(StartPage.SearchString);
            }
        @And("I press the Search button")
        public void I_Press_The_Search_Button() throws InterruptedException
        {
            Button_Search.click();
            driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
            assertTrue("Search button at result page is visible.",ResultButton_Search2.isDisplayed());
            assertTrue("Search button at result page is not visible.",ResultButton_Search2.isDisplayed());
            assertTrue("Search button at result page is not visible.",Image_LogoResult.isDisplayed());
            assertTrue("Search button at result page is not visible.",FoundText.isDisplayed());
            Assert.assertTrue("Found results value is empty."+
                            FoundText.getText().split(" ")[1].toString()+"",
                    !(FoundText.getText().split(" ")[1].toString()==""));
            //driver = SearchTest(driver);
        }

        @Test
        public void TestTest() throws InterruptedException {
            I_On_Yandex_Start_Page();
            I_Enter_text_To_Textbox();
            I_Press_The_Search_Button();
        }

        @Test
        public void Main()
        {
            assertTrue("Expected element is not visible at page.",Image_Logo.isDisplayed() );

            try {
                driver = SearchTest(driver);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            assertTrue("Search button at result page is not visible.",ResultButton_Search2.isDisplayed());
            assertTrue("Search button at result page is not visible.",Image_LogoResult.isDisplayed());
            assertTrue("Search button at result page is not visible.",FoundText.isDisplayed());
            Assert.assertTrue("Found results value is empty."+
                    FoundText.getText().split(" ")[1].toString()+"",
                    !(FoundText.getText().split(" ")[1].toString()==""));
        }

        public WebDriver SearchTest(WebDriver driver) throws InterruptedException {
            assertTrue("Expected element is visible at page.",Image_Logo.isDisplayed() );
            TextBox_Search.clear();
            TextBox_Search.sendKeys(SearchString);
            Button_Search.click();
            driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
            assertTrue("Search button at result page is visible.",ResultButton_Search2.isDisplayed());
            return driver;
        }
    }
