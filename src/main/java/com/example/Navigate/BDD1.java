package com.example.Navigate;

import cucumber.api.java.en.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by andrus on 5/25/2016.
 */

public class BDD1 extends PageInstance
    {
        public static String BaseURL;
        public static String SearchString;
        static public WebDriver driver;

        public BDD1()  {
            try {
                driver = new FirefoxDriver();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            PageFactory.initElements(driver, this);
        }

        @Before
        public void setUp(){
            BaseURL = "https://www.google.com.ua";
            SearchString="tetracyclines list";
            driver.get(BaseURL);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }

        @FindBy(id = "hplogo")
        static public WebElement Image_Logo;//Start page.
        @FindBy (xpath= ".//input[@type='text']")
        static public WebElement TextBox_Search;
        @FindBy (xpath = ".//center/input[1]")
        static public WebElement Button_Search;
        @FindBy (xpath = ".//button[@type='submit']")
        static public WebElement ResultButton_Search2;
        @FindBy (xpath = ".//*[@id='logo']/img")
        public WebElement Image_LogoResult;//Results page.
        @FindBy (xpath = ".//*[@id='resultStats']")
        public WebElement FoundText;
        @FindBy (xpath = ".//div[2]/b[contains(text(),'antibiotics')]")
        public WebElement FoundDropdownTetracyclinesAntibioticsList;


        @Given("I on Yandex Start page$")
        public void I_On_Google_Start_Page() throws InterruptedException
            {
                Assert.assertTrue("Логотип не отображается.",Image_Logo.isDisplayed());
                Assert.assertTrue("Строка поиска не отображается.",TextBox_Search.isDisplayed());
                Assert.assertTrue("Кнопка 'Search' не отображается.",Button_Search.isDisplayed());
            }
        @Then("I enter text *** to textbox$")
        public void I_Enter_text_To_Textbox() throws InterruptedException
            {
                TextBox_Search.clear();
                TextBox_Search.sendKeys(SearchString);
                driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
            }
        @And("I press the Search button")
        public void I_Press_The_Search_Button() throws InterruptedException
        {
            FoundDropdownTetracyclinesAntibioticsList.click();
            //Button_Search.click();
            driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
            assertTrue("Search button at result page is visible.",ResultButton_Search2.isDisplayed());
            assertTrue("Search button at result page is not visible.",ResultButton_Search2.isDisplayed());
            assertTrue("Search button at result page is not visible.",Image_LogoResult.isDisplayed());
            assertTrue("Search button at result page is not visible.",FoundText.isDisplayed());
            Assert.assertTrue("Found results value is empty."+
                      FoundText.getText().split(":")[1].toString().split(" ")[1]+FoundText.getText().split(":")[1].toString().split(" ")[2]+"",
                    !(FoundText.getText().split(":")[1].toString().split(" ")[1]+FoundText.getText().split(":")[1].toString()==""));
            //driver = SearchTest(driver);
        }

        @Test
        public void TestTest() throws InterruptedException {
            I_On_Google_Start_Page();
            I_Enter_text_To_Textbox();
            I_Press_The_Search_Button();
        }

        @Test
        public void Main()
        {
            assertTrue("Expected element is not visible at page.",Image_Logo.isDisplayed() );

            try {
                driver = SearchTest(driver);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            assertTrue("Search button at result page is not visible.",ResultButton_Search2.isDisplayed());
            assertTrue("Search button at result page is not visible.",Image_LogoResult.isDisplayed());
            assertTrue("Search button at result page is not visible.",FoundText.isDisplayed());
            Assert.assertTrue("Found results value is empty."+
                    FoundText.getText().split(" ")[1].toString()+"",
                    !(FoundText.getText().split(" ")[1].toString()==""));
        }

        public WebDriver SearchTest(WebDriver driver) throws InterruptedException {
            assertTrue("Expected element is visible at page.",Image_Logo.isDisplayed() );
            TextBox_Search.clear();
            TextBox_Search.sendKeys(SearchString);
            Button_Search.click();
            driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
            assertTrue("Search button at result page is visible.",ResultButton_Search2.isDisplayed());
            return driver;
        }
    }
