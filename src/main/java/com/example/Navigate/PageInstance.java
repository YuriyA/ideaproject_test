package com.example.Navigate;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created on 5/16/2016.
 */


public class PageInstance {
    static public WebDriver driver;

    @Before
    public void setUp(){
        try {
            driver = new FirefoxDriver();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    ResultsPage resultsPage;
    @Test
    @Given("I on Yandex Start page$")
    public void I_On_Yandex_Start_Page() throws Throwable
    {

    }

    @Test
    @Then("I enter text *** to textbox$")
    public void I_Enter_text_To_Textbox() throws Throwable
    {
        StartPage.TextBox_Search.clear();
    }

    @Test
    @And("I press the Search button")
    public void I_Press_The_Search_Button() throws Throwable
    {
    }

    //public abstract void I_On_Yandex_Start_Page();

}

