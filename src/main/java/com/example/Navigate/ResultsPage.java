package com.example.Navigate;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created on 5/16/2016.
 */
public class ResultsPage {
    static public String PageSource;
    static public WebDriver driver;

    public ResultsPage() throws InterruptedException {
        StartPage.SearchTest(driver);
    }

    /*public ResultsPage(WebDriver driver) throws InterruptedException {
    StartPage.SearchTest(driver);
    }/**/

    @Before
    public void setUp(){
        PageSource = driver.getPageSource();
    }

    @Test
    public void TestForResultsPage() throws InterruptedException {
        assertTrue("Logo element is visible at page.",Image_Logo.isDisplayed() );
        assertTrue("'Found Text' element is visible at page.",FoundText.isDisplayed() );
        assertNotNull("'FoundText' element contains results:",FoundText.getText().split(" ")[1]);
        assertNotEquals("'FoundText' element contains results:",0,FoundText.getText().split(" ")[1]);
    }

    @FindBy (xpath = "html/body/div[1]/div/div[1]/div[1]/div/a")
    public WebElement Image_Logo;
    @FindBy (xpath = ".//div[@class='input__found input__found_visibility_visible']")
    public WebElement FoundText;
/*
    @FindBy(xpath = "html/body/table/tbody/tr[3]/td/a")
    static public WebElement Image_Logo;
    @FindBy (id = "text")
    static public WebElement TextBox_Search;
    @FindBy (xpath = "html/body/table/tbody/tr[2]/td/form/div[2]/button")
    static public WebElement Button_Search;
    @FindBy (xpath = "//div[@class='search2__button']")
    static public WebElement ResultButton_Search2;/**/

    @After
    public void tearDown(){
        driver.quit();
    }

    @Ignore
    public void Test3(){}
}
