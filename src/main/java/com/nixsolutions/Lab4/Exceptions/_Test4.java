package com.nixsolutions.Lab4.Exceptions;

import org.apache.xpath.operations.Bool;
import org.junit.Test;

import java.io.*;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.io.FileWriter;

/**
 * class _Test4
 * <p>
 * Srtings
 * <p>
 * 5/21/2016
 * <p>
 * Created by andrus.
 */

public class _Test4 {
    public _Test4() {
    }

    public void Save(String fileName) throws FileAlreadyExistsException, IOException {
        Bool bool;
        int i;
        String newFileCreated;
        File exist = new File(fileName);
        exist.delete();
        if (exist.exists())
            throw new FileAlreadyExistsException(fileName);
        exist.createNewFile();
        FileWriter writeFile = new FileWriter(exist);
        writeFile.write("test1");
        writeFile.write("\n");
        writeFile.write("test2");
        writeFile.flush();
        writeFile.close();
    }

    @Test
    public void Main() {
        String fileName = "d:\\_work\\notes3.txt";
        try {
            Save(fileName);
        } catch (FileAlreadyExistsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
