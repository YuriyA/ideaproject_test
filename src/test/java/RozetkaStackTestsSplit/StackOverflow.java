package RozetkaStackTestsSplit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created on 6/13/2016.
 */
public class StackOverflow {
    public StackOverflow() {
        String StackOverflowURL;
        StackOverflowURL = "https://stackoverflow.com/";
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(StackOverflowURL);
        driver.manage().window().maximize();
        PageFactory.initElements(driver, this);
    }
    public WebDriver driver;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        driver.quit();
    }

        @FindBy(xpath = ".//span[@class='bounty-indicator-tab']")
        public WebElement FeaturedNumber;
        @FindBy(xpath = "//a[contains(text(),'sign up')]")
        public WebElement SignUpLink;
        @FindBy(id = "hlogo")
        public WebElement Image_Logo;
        @FindBy(xpath = ".//span[contains(text(),'Google')]")
        public WebElement GoogleLink;
        @FindBy(xpath = ".//span[contains(text(),'Facebook')]")
        public WebElement FaceBookLink;
        @FindBy(xpath = ".//div[1]/div[2]/h3/a")
        public WebElement FirstQuestionLink;
        @FindBy(xpath = ".//*[@id='qinfo']/tbody/tr[1]/td[2]/p/b")
        public WebElement QuestionAsked;
        @FindBy(id = "nav-jobs")
        public WebElement JobsLink;
        @FindBy(xpath = ".//span[@class='salary'][contains(text(),',')]")
        public WebElement SalaryText;

    @Test
    public void TestLogo() throws InterruptedException {
        System.out.println("StackOverflow tests are started.");
        assertTrue("Expected element is not visible at page.", Image_Logo.isDisplayed());
    }

    @Test
    public void TestNumberOfPublications() throws InterruptedException {
        assertTrue("Number of featured publications at page lower than 300.", (Integer.parseInt(FeaturedNumber.getText()) >= 300));
    }

    @Test
    public void TestSignUpOver() throws InterruptedException {
        SignUpLink.click();
        assertTrue("Google Link is not visible at page.", GoogleLink.isDisplayed());
        assertTrue("FaceBook Link is not visible at page.", FaceBookLink.isDisplayed());
        System.out.println("StackOverflow tests are finished.");
    }

    @Test
    public void TestFirstQuestionDate() throws InterruptedException {
        FirstQuestionLink.click();
        assertTrue("Last question date is not today.", QuestionAsked.getText().matches("today"));
    }

    @Test
    public void TestSalary() throws InterruptedException {
        Integer minSalary = 99000, i=0;
        Boolean MoreThan100000 = false;
        List<WebElement> SalariesList;
        JobsLink.click();

        SalariesList = driver.findElements(By.xpath(".//span[@class='salary'][contains(text(),',')]"));
        for (WebElement elem : SalariesList) {//new - with regexp.
            List<String> list = regex.RegExMethods.getMatchedArray(elem.getText(), "\\d+.\\d+");
            i++;
            for (String salaries : list) {
                StringBuffer sb = new StringBuffer(salaries);
                sb.deleteCharAt(sb.indexOf(","));
                if (minSalary <= Integer.valueOf(sb.toString().trim())) {
                    System.out.println("Offer value >=100.000 " + salaries + " is present. Salary is "+sb.toString().trim()+".");
                    MoreThan100000=true;
                }
            } if (MoreThan100000) {
                assertTrue("Salary more than or equal 100 000 was not found.", MoreThan100000);
                return;};
        }

    }
}