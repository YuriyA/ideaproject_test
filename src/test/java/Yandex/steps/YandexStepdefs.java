package Yandex.steps;

import Yandex.Mapping.StartPage;
import Yandex.Mapping.ResultPage;
import Yandex.runners.YandexRun;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by andrus on 5/31/2016.
 */
public class YandexStepdefs {
    public static String BaseURL;
    public static String SearchString;


    @Given("^I on Yandex page$")
    public void iOnYandexPage() throws Throwable {
        BaseURL = "https://www.google.com.ua";

        Assert.assertTrue("Логотип не отображается.", YandexRun.startPage.Image_Logo.isDisplayed());
        Assert.assertTrue("Строка поиска не отображается.",YandexRun.startPage.TextBox_Search.isDisplayed());
        Assert.assertTrue("Кнопка 'Search' не отображается.",YandexRun.startPage.Button_Search.isDisplayed());
    }

    @When("^I enter search text \"([^\"]*)\"$")
    public void iEnterSearchText(String arg0) throws Throwable {
        SearchString="tetracyclines list";
        YandexRun.startPage.TextBox_Search.clear();
        YandexRun.startPage.TextBox_Search.sendKeys(SearchString);
    }

    @And("^I click the button \"([^\"]*)\"$")
    public void iClickTheButton(String arg0) throws Throwable {
        YandexRun.resultPage.FoundDropdownTetracyclinesAntibioticsList.click();
        //Button_Search.click();



    }

    @Then("^I see more then first (\\d+) results on page$")
    public void iSeeMoreThenFirstResultsOnPage(int arg0) throws Throwable {
        //assertTrue("Search button at result page is not visible.",FoundText.isDisplayed());
        /*Assert.assertTrue("Found results value is empty."+
                        FoundText.getText().split(":")[1].toString().split(" ")[1]+FoundText.getText().split(":")[1].toString().split(" ")[2]+"",
                !(FoundText.getText().split(":")[1].toString().split(" ")[1]+FoundText.getText().split(":")[1].toString()==""));*/
    }
}
