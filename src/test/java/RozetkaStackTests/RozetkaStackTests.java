package RozetkaStackTests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created on 6/9/2016.
 */
public class RozetkaStackTests {
    public RozetkaStackTests() {
    };
    public static String RozetkaURL, StackOverflowURL;
    private static String RozetkaLogoXpath, RozetkaAppleXpath, RozetkaMP3Xpath;
    private static WebElement RozetkaLogoElement;
    static public WebDriver driver;

    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        System.out.println("All tests setup.");
    }

    @Test
    public void Alltests() throws InterruptedException {
        Rozetka.RozetkaTest();
        StackOverflow.StackOverflowTest();
    }

    ;

    @After
    public void tearDown() {
        driver.quit();
    }

    public static class Rozetka {

        public Rozetka() {
        }

        public static void TestLogo() throws InterruptedException {
            RozetkaURL = "http://rozetka.com.ua/";
            RozetkaAppleXpath = ".//*[@id='m-main']/li[2]/a[contains(text(),'Apple')]";
            RozetkaMP3Xpath = ".//*[@id='m-main']/li[2]/a[contains(text(),'Apple')]";
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(RozetkaURL);
            driver.manage().window().maximize();
            RozetkaLogoXpath = ".//*[@id='body-header']/div[2]/div/div[2]/div[1]/img";
            RozetkaLogoElement = driver.findElement(By.xpath(RozetkaLogoXpath));
            assertTrue("Rozetka logo element is not visible at page.", RozetkaLogoElement.isDisplayed());
        }

        public static void TestApple() throws InterruptedException {
            WebElement RozetkaAppleElement = driver.findElement(By.xpath(RozetkaAppleXpath));
            assertTrue("Apple menu item element is not visible at page.", RozetkaAppleElement.isDisplayed());
            RozetkaAppleElement.isDisplayed();
        }

        public static void TestMP3() throws InterruptedException {
            WebElement RozetkaMP3Element = driver.findElement(By.xpath(RozetkaMP3Xpath));
            assertTrue("Menu elements with 'MP3' text was not found.", RozetkaMP3Element.isDisplayed());
            RozetkaMP3Element.isDisplayed();
        }

        public static void TestCityNames() throws InterruptedException {
            WebElement CityChooserMenu = driver.findElement(By.xpath(".//*[@id='city-chooser']/a/span"));
            assertTrue("City Chooser Menu is not displayed.", CityChooserMenu.isDisplayed());
            CityChooserMenu.click();
            WebElement CityKharkov = driver.findElement(By.partialLinkText("Харьков"));
            WebElement CityOdessa = driver.findElement(By.partialLinkText("Одесса"));
            WebElement CityKyiv = driver.findElement(By.partialLinkText("Киев"));
            CityKharkov.isDisplayed();
            CityOdessa.isDisplayed();
            CityKyiv.isDisplayed();
        }

        public static void TestBinContents() throws InterruptedException {
            WebElement Bin = driver.findElement(By.partialLinkText("Корзина"));
            assertTrue("Bin link is not displayed.", Bin.isDisplayed());
            Bin.click();
            WebElement BinEmpty = driver.findElement(By.xpath(".//h2[text()='Корзина пуста']"));
            BinEmpty.isDisplayed();
        }

        public static void RozetkaTest() throws InterruptedException {
            System.out.println("Rozetka tests are started.");
            TestLogo();
            TestApple();
            TestMP3();
            TestCityNames();
            TestBinContents();
            System.out.println("Rozetka tests are finished.");
        }
    }

    public static class StackOverflow {

        public StackOverflow() {
        }

        public static void StackOverflowTest() throws InterruptedException {
            System.out.println("StackOverflow tests are started.");
            StackOverflowURL = "https://stackoverflow.com/";
            driver.get(StackOverflowURL);
            driver.manage().window().maximize();
            WebElement FeaturedNumber = driver.findElement(By.xpath(".//*[@id='tabs']/a[2]/span"));
            WebElement SignUpLink = driver.findElement(By.xpath("html/body/div[4]/div/div[3]/div[1]/span/a[1]"));
            WebElement Image_Logo = driver.findElement(By.id("hlogo"));
            Image_Logo.isDisplayed();
            assertTrue("Expected element is not visible at page.", Image_Logo.isDisplayed());
            assertTrue("Number of featured publications at page lower than 300.", (Integer.parseInt(FeaturedNumber.getText()) >= 300));
            SignUpLink.click();
            WebElement GoogleLink = driver.findElement(By.xpath(".//*[@id='openid-buttons']/div[1]/div[2]/span[contains(text(),'Google')]"));
            WebElement FaceBookLink = driver.findElement(By.xpath(".//*[@id='openid-buttons']/div[2]/div[2]/span[contains(text(),'Facebook')]"));
            assertTrue("Google Link is not visible at page.", GoogleLink.isDisplayed());
            assertTrue("FaceBook Link is not visible at page.", FaceBookLink.isDisplayed());
            Image_Logo = driver.findElement(By.id("hlogo"));
            Image_Logo.click();
            WebElement FirstQuestionLink = driver.findElement(By.xpath(".//div[1]/div[2]/h3/a"));
            FirstQuestionLink.click();
            WebElement QuestionAsked = driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]/td[2]/p/b"));
            assertTrue("Last question date is not today.", QuestionAsked.getText().matches("today"));
            System.out.println("StackOverflow tests are finished.");
            System.out.println("All tests are finished.");
        }

    }
}