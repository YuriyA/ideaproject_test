package com.example.Navigate;

import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.*;
import org.junit.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created on 5/16/2016.
 */
/*public class Background {
    static public String PageSource, Text;
    static public WebDriver driver;
    public static String BaseURL;
    public static StartPage startPage;

 //   @Before
    public void setUp(){
        Background navigate = new Background();
    }


  //  @Test
    public void Test1() throws Exception {
        try {
            driver = new FirefoxDriver();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://stackoverflow.com");
        PageSource = driver.getPageSource();
        Text = driver.findElement(By.id("h-top-questions")).getText();
        assertFalse("Header with 'h-top-questions' id does not contains the expected string.",!Text.matches("Top Questions"));
        assertTrue("Page contains expected string.", PageSource.toString().contains("Top Questions"));
        driver.quit();
    }

/*  @Test
    public void Test2() throws Exception {
        try {
            driver = new FirefoxDriver();
        } catch (Exception e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://stackoverflow.com");
        driver.manage().window().maximize();
        PageSource = driver.getPageSource();
        Text = driver.findElement(By.id("h-top-questions")).getText();
        assertTrue("Page contains expected string.", PageSource.toString().contains("Top Questions"));
        assertFalse("Header with 'h-top-questions' id does not contains the expected string.",!Text.matches("Top Questions"));
        driver.findElement(By.id("tell-me-more")).click();
        TimeUnit.SECONDS.sleep(10);
        PageSource = driver.getPageSource();
        Text = driver.findElement(By.id("more-options-link")).getText();
        assertTrue("'Sign Up' button does not contains the expected string.",Text.matches("more sign up options"));
    }/**/
/*
    @After
    public void tearDown(){
        driver.quit();
    }

    @Ignore
    public void Test3(){}/**/

/*    @UiThread
    public void ddd(){
        WebDriver driver1;
        BaseURL = "https://www.ukr.net/";
        driver1=new InternetExplorerDriver();
        driver1.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        startPage = new StartPage();
        driver1.get(BaseURL);
        driver1.manage().window().maximize();
    }

}
/**/
