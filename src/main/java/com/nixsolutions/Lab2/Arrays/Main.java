package com.nixsolutions.Lab2.Arrays;

import javafx.scene.paint.Color;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.openqa.jetty.html.Input.File;

/**
 * class _Test2
 * <p>
 * V1
 * <p>
 * 5/25/2016
 * <p>
 * Created by andrus.
 */

public class Main {
    public double markerf20consumption = 10;
    public double markerf2140consumption = 10;
    public double markerf41consumption = 10;

    public double pencilSharpingconsumption = 3;
    public double pencilSharpingNeededSymbols = 20;

    Integer[] listTools = new Integer[10];
    public StringBuilder tmp;

    @Before

    @Test
    public void main() {
        tmp = new StringBuilder();
        String[] listToolNames = new String[]{"Pen", "Pencil", "Marker"};
        Double[] listToolBudget = new Double[]{0.0, 0.0, 0.0};//Number of written symbols.
        Double[] listToolSizes = new Double[]{10.0, 10.0, 10.0};
        Double[] listToolConcumptions = new Double[]{0.15, 0.095, 0.01};//
        listTools = formingTools();
        for (Integer Tool : listTools) {
            tmp = formingString();
            listToolBudget[Tool] += (tmp.length());
            switch (listToolNames[Tool]) {
                case "Pen":
                    listToolSizes[Tool] -= (tmp.length() * listToolConcumptions[Tool]);
                    System.out.print("Pen: " + tmp + " " + listToolSizes[Tool] + "\n");
                    break;
                case "Pencil":
                    listToolSizes[Tool] -= (tmp.length() * listToolConcumptions[Tool]);
                    if (listToolBudget[Tool] >= 20) {
                        listToolSizes[Tool] -= 0.03;
                        listToolBudget[Tool] -= 20;
                    }
                    System.out.print("Pencil: " + tmp + " " + listToolSizes[Tool] + "\n");
                    break;
                case "Marker":
                    if (listToolBudget[Tool] <= 20) {
                        listToolSizes[Tool] -= (tmp.length() * 0.01);
                        break;
                    }
                    if (listToolBudget[Tool] > 20 && listToolBudget[Tool] <= 40) {
                        listToolSizes[Tool] -= (tmp.length() * 0.0109);
                        break;
                    }
                    if (listToolBudget[Tool] > 40) {
                        listToolSizes[Tool] -= (tmp.length() * 0.015);
                        break;
                    }
                    System.out.print("Marker: " + tmp + " " + listToolSizes[Tool] + "\n");
                    break;
            }

        }
        int i = 0;
    }

    Double[] writingLetters(Double[] listToolSizes) {

        return listToolSizes;
    }

    Integer[] formingTools() {
        Integer index = 0;
        Random randomTool = new Random();
        for (Integer Tool : listTools) {
            listTools[index] = (randomTool.nextInt(3));
            index++;
        }
        return listTools;
    }

    StringBuilder formingString() {
        StringBuilder string = new StringBuilder();
        string.append("1234567890");
        return string;
    }
}

class Pen extends Writer {
    private boolean Enable;

    public void write(String str, StringBuilder stringBuilder) {
    }

    public void erase(String str, StringBuilder stringBuilder) {
    }

    public void expected(String str) {
    }
}

class Writer {
    //        private Color color;
    public interface Writable {
        public void write(String str);
    }
}

class Pencil extends Writer {
    public void write(String str, StringBuilder stringBuilder) {
    }

    public void erase(String str, StringBuilder stringBuilder) {
    }

    public void expected(String str) {
    }
}

class Marker extends Writer {
    public void write(String str, StringBuilder stringBuilder) {
    }

    public void erase(String str, StringBuilder stringBuilder) {
    }

    public void expected(String str) {
    }


}/**/