package Yandex.runners;

import Yandex.Mapping.StartPage;
import Yandex.Mapping.ResultPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

//import javax.security.auth.login.Configuration;


/**
 * Created by andrus on 5/31/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/smoketest", "json:target/cucumber.json"},
        features = "src/test/java/Yandex/features",
        glue = "ru/Yandex/steps",
        tags = "@smoketest")

public class YandexRun {

        protected static WebDriver driver;
        public static ResultPage resultPage;
        public static StartPage startPage;

        @BeforeClass
        public static void setupTimeout(){
            driver=new FirefoxDriver();
            startPage = new StartPage(driver);
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        }

        @AfterClass
        public static void TearDown(){
            driver.close();
        }
}
