package AltTestIncludingTableData.runners;
import AltTestIncludingTableData.Mapping.ResultPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.*;
import java.nio.CharBuffer;
import java.util.Set;


/**
 * Created by andrus on 5/31/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/smoketest", "json:target/cucumber.json"},
        features = "src/test/java/AltTestIncludingTableData/features",
        glue = {"AltTestIncludingTableData/Steps","java/AltTestIncludingTableData/Steps"},
        tags = {"@smoketest"/*,"@alltests", "@special"*/})

public class AltRunner {

        @BeforeClass
        public static void setupTimeout(){
            FileReader fileReader= null;
            try {
                fileReader = new FileReader("d:\\_work\\test.txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String s1;
            s1=fileReader.toString();
            char[] cbuf=new char[16];
            try {
                fileReader.read(cbuf,0,16);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @AfterClass
        public static void TearDown(){
            Set<String> FFWindowsSet = ResultPage.driver.getWindowHandles();
            String newWindowHandle = FFWindowsSet.iterator().next();
            ResultPage.driver.switchTo().window(newWindowHandle);
            ResultPage.driver.close();
         }

}
